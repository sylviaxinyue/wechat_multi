# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 20:49:50 2021

@author: Administrator
"""

import requests
from datetime import datetime
import datetime

key = "dbeb0502639ec115136dc52002f8596b"


def main():
    print(get_holiday_date())

def get_holiday_date():
    url = "http://api.tianapi.com/txapi/jiejiari/index"
    now_da = datetime.datetime.now()
    now_date = now_da.strftime("%Y-%m-%d")
    now_date_plus = datetime.datetime.now() + datetime.timedelta(days=30)
    now_date_plus_one = now_date_plus.strftime("%Y-%m-%d")
    date = now_date+'~'+now_date_plus_one
    params = {
        "key": key,  # 应用APPKEY(应用详细页查询)
        "date":date,
        'type':3
    }

    # try:
    r = requests.get(url = url,params = params)
    r.raise_for_status()
    r.encoding = r.apparent_encoding
    res = r.json()
    w = res["newslist"]
    re_list = []
    for i in w:
        switch = str(i['daycode'])
        if switch != '1':
            pass
        else:
            holiday =i['holiday']
            name = i['name']
            tip = i['tip']
            rest = i['rest']
            re = '* %s * %s\n调休安排:%s\n小贴士:%s'%(holiday,name,tip,rest)
            re_list.append(re) 
    res_list = list(set(re_list)) 
    resul = '\n\n'.join(res_list)
    str_new = resul.replace(" ", "")
    if str_new == '':
        return '接下来一个月没有调休假日'
    else:
        result = '接下来一个月内的调休假日:\n'+resul
        return result
    # except:
    #     return '糟了!访问出错,赶紧找大欣悦来解决问题'
        
if __name__ == '__main__':
    main()