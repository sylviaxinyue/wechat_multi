# -*- coding: utf-8 -*-
# @Time    : 2020/6/4
# @Author  : Sylvia
# @Email   : 1401965853@qq.com
# @Software: Spyder


import threading
import function_object
import json
import urllib.request
import time
import logging
import sys
import random
import newsReturn
import lotteryReturn
import historyReturn
import almanacReturn
import fortuneReturn
import choiceReturn
import topsearchhttp
import weatherreturn
import tophubReturn
import sleepstoryReturn
import paperworkReturn
import holidayReturn
import tornado.ioloop
from wechatusersql import MCsql
from queue import Queue
from WechatPCAPI import WechatPCAPI
from enum import Enum
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.date import DateTrigger
from apscheduler.schedulers.tornado import TornadoScheduler
from datetime import datetime
import datetime


#全局变量
api_key = '2a22ca3cc8e54c0790e8b26bbd382820'   #图灵机器人apiKey
user_id = '672653'    #图灵机器人账户userId
api_url = "http://openapi.tuling123.com/openapi/api/v2"    #图灵机器人post调用接口

menustr = '''功能如下，只要回复【】内数字就可以开启相关内容
通用功能
【1】和小欣悦机器人聊天
【2】选择困难症患者请进
【3】常见彩票开奖信息
【4】天气查询
【5】历史上的今天
【6】实时新闻浏览
【7】翻翻老黄历
【8】星座运势
【9】热榜资讯
【10】朋友圈文案生成器
【11】近期调休安排
【666】开发者微信
个人功能
【01】个人信息
【02】订阅
【03】备忘录(暂未开放)
'''

subscriptionstr = '''【1】订阅此功能
【2】修改发送时间
【3】取消订阅此功能
【0】返回主菜单
'''


#菜单状态变量，确认用户在哪级菜单
class state_list(Enum):
    main = 0
    secondary_robot = 1
    secondary_choice = 2
    secondary_user_surnom = 3
    secondary_city = 4
    secondary_weather = 44
    secondary_user_signs = 5
    secondary_user_birthday = 43
    secondary_news = 6
    secondary_fortune = 8
    secondary_broadcast = 946743
    secondary_tophub = 9
    #订阅菜单
    secondary_subs_lottery = 22001
    secondary_subs_weather = 22002
    secondary_subs_sleep = 22003
    secondary_subs_almanac = 22004
    secondary_subs_fortune = 22005
    secondary_subs_topbaidu = 22006
    secondary_subs_tiangou = 22007
#原有的消息队列内容
logging.basicConfig(level=logging.INFO)
queue_recved_message = Queue()

#获取dict的消息内容，并且放到消息队列中
def on_message(message):
    queue_recved_message.put(message)
#两个回调函数，作为数据参数，一个是收到消息后的处理
wx_inst = WechatPCAPI(on_message=on_message, log=logging)

#获取图灵反馈信息函数调用，message为传入的文本信息
def get_message(message):
    #图灵机器人post json消息体
    global api_key
    global user_id
    req = {
    "reqType":0,
    "perception":
        {
            "inputText":
            {
                "text": message
            },
        },
    "userInfo": 
        {
            "apiKey": api_key,
            "userId": user_id
        }
    }
    #按照utf8格式对req(dict)进行json格式编码
    req = json.dumps(req).encode('utf8')
    #按照request方式进行post数据通讯
    http_post = urllib.request.Request(api_url, data=req, headers={'content-type': 'application/json'})
    #用于打开一个远程的url连接,并且向这个连接发出请求,获取响应结果
    response = urllib.request.urlopen(http_post)
    response_str = response.read().decode('utf8')
    #读取字符串数据转为dict
    response_dic = json.loads(response_str)
    #读取json key分别嵌套的results values text 得到回复信息
    results_text = response_dic['results'][0]['values']['text']
    print(results_text)  
    return results_text


    
def get_tbd(user, mes):
    wx_inst.send_text(user, "目前还未支持此功能，请稍后")
    return True

    
    
#主菜单返回字符串，主菜单对象及对象集合
main_dict = {
'0':'''【主菜单】
''' + menustr,

'1':'''【机器人对话】
你好，我是小欣悦，现在起可以和我聊天啦！
如果想返回主菜单，请发送数字【0】
''',

'2':'''【选择困难症】
平时很难下决定?小事情交给我吧!
输入【】内对应数字进入
【1】今天吃什么?
【2】上海上海去哪儿玩?
【3】《答案之书》解你心结
【4】Yes or No ??
【0】返回主菜单
''',

'4':'''请发送城市名称来查询天气
如要结束查询,【0】返回主菜单
''',

'昵称':'''想让我怎么称呼你呢?请直接发送
如不想修改,则【0】返回主菜单
''',

'城市':'''请直接发送城市名称来修改位置信息
如不想修改,则【0】返回主菜单
''',

'星座':'''请直接发送你的星座(xx座)来修改星座信息
如不想修改,则【0】返回主菜单
''',

'生日':'''请直接发送你的生日(例:1月1日发送【0101】)来添加生日信息
如不想添加,则【0】返回主菜单
''',

'6':'''【新闻浏览】
输入【】内对应数字进入
【1】随便来点
【2】社会
【3】国内
【4】国际
【5】娱乐
【6】体育
【7】军事
【8】科技
【9】财经
【10】时尚
【0】返回主菜单
''',

'8':'''【星座运势】
输入【】内对应数字进入
【1】白羊座(3.21-4.19)
【2】金牛座(4.20-5.20)
【3】双子座(5.21-6.21)
【4】巨蟹座(6.22-7.22)
【5】狮子座(7.23-8.22)
【6】处女座(8.23-9.22)
【7】天秤座(9.23-10.23)
【8】天蝎座(10.24-11.22)
【9】射手座(11.23-12.21)
【10】摩羯座(12.22-1.19)
【11】水瓶座(1.20-2.18)
【12】双鱼座(2.19-3.20)
【0】返回主菜单
''',

'9':'''【热榜资讯】
高效获取最新最热资讯,让你一秒读懂朋友圈!
输入【】内对应数字进入
热点类：
【1】百度：实时热点
【2】今日头条：头条热榜
科技类：
【3】IT之家：日榜
【4】好奇心日报：大公司头条top15
【5】威锋网：今日新信息
财经类：
【6】财新网：评论热榜
【7】新浪财经新闻：点击量排行
【8】华尔街见闻：日排行
影视书籍类：
【9】电影新片榜
【10】热门剧集榜
【11】在映电影
【12】微信读书：飙升榜
娱乐类：
【13】网易娱乐：点击榜
【14】新浪：娱乐榜
游戏类：
【15】3DM游戏网：最新新闻
【16】机核网：每日最新
体育类：
【17】新浪：体育榜
【0】返回主菜单
''',

'946743':'''开发者向所有用户的广播
''',

'22001':'''【22001】每日彩票开奖信息
''' + subscriptionstr,

'22002':'''【22002】本地天气预报
查询位置依据你填写的个人信息,如需要更改地区请先【0】返回主界面后,发送【01】进入个人信息的修改
''' + subscriptionstr,

'22003':'''【22003】提醒睡觉功能
我会在你设定的时间提醒你睡觉!都是成年人了,定好几点睡觉要说话算话哦~
''' + subscriptionstr,

'22004':'''【22004】黄历
每日凶吉知多少,黄道吉日速知晓!
''' + subscriptionstr,

'22005':'''【22005】星座运势
星座查询依据你填写的个人信息,请确保已填写,如未填写请先【0】返回主界面后,发送【01】进入个人信息的修改,否则将无法推送
''' + subscriptionstr,

'22006':'''【22006】热点号角
自动为你推送综合热点,其他热点内容的查询可以在【0】返回主界面后,发送【9】进入
''' + subscriptionstr,

'22007':'''【22007】舔狗日记
'她总有一天会喜欢我'
''' + subscriptionstr,

}
#通用回调函数
def exit_all_function(user, mes):#回到主菜单
    global g_state
    g_state = state_list.main
    mcsql.add_location('0',now_username)#数据库记录

#gbk不可传输字符易出现使用的函数
def tryprint(user,defs):
    try:
        wx_inst.send_text(user,defs)
    except UnicodeEncodeError:
        wx_inst.send_text(user, '内容出现不可传输字符,请重试')

#机器人菜单回调函数集合
def entry_robot_function(user, mes):
    global g_state
    g_state = state_list.secondary_robot
    mcsql.add_location('1',now_username)#数据库记录

def custom_robot_function(user, mes):
    wx_inst.send_text(user, "欣悦是小可爱!")
    
def robot_process_function(user, mes):
    res_robot = get_message(mes)
    wx_inst.send_text(user, res_robot)
    
#选择菜单回调函数集合
def entry_choice_function(user, mes):
    global g_state
    g_state = state_list.secondary_choice
    mcsql.add_location('2',now_username)#数据库记录
def choice_process_function(user, mes):
    msg_content = mes
    
    if msg_content == '1' :
         wx_inst.send_text(user,'我觉得你可以去吃:%s' % choiceReturn.style_cooking())
         wx_inst.send_text(user, "共有45种菜品类型,如对结果不满意,可发送【1】重新获取,【0】返回主菜单")
    elif msg_content == '2' :
         wx_inst.send_text(user,'我觉得可以去:%s' % choiceReturn.district_shanghai())
         wx_inst.send_text(user, "为你选择了区域与商场,如对结果不满意,可发送【2】重(zai)新(jia)获(xiu)取(xi),【0】返回主菜单")
    elif msg_content == '3' :
         wx_inst.send_text(user, 
'''《答案之书》使用说明。
1.把手机放在桌上,闭上眼睛。
2.用5到10秒的时间集中思考你的问题。
3.在想象或说出你的问题的同时(每次只能有一个问题)在输入框打上【咋办呢】。
4.在你感觉时间正确的时候点击发送,你要寻找的答案就被我捯饬来了。
5.遇到任何问题你都可以来试试。
''')
    elif msg_content == '4' :
        wx_inst.send_text(user,'%s'%choiceReturn.yn())
    elif msg_content == '咋办呢' :
        wx_inst.send_img(user,'%s'%choiceReturn.answer_book())
        wx_inst.send_text(user, "天灵灵地灵灵~~~就是你了!")                  
    else:
        wx_inst.send_text(user,'%s' % "请输入功能对应的数字查询喔,【0】返回主菜单")
                     
    return True          

     
#彩票菜单回调函数集合   
def get_lottery_function(user, mes):
    wx_inst.send_text(user, lotteryReturn.lottery_all())
    
#天气菜单回调函数集合
def entry_weather_function(user, mes):
    global g_state
    g_state = state_list.secondary_weather
    mcsql.add_location('44',now_username)#数据库记录
def weather_process_function(user, mes):
    msg_content = mes
    weather_api_return = weatherreturn.futureWeather(msg_content)
    if weather_api_return == '0':
        wx_inst.send_text(user, "请输入正确城市名称")
    else:
        weather_city = msg_content
        tryprint(user, weatherreturn.fetchWeather(weather_city))
        tryprint(user, weatherreturn.futureWeather(weather_city))
        wx_inst.send_text(user, '输入城市名称继续查询,【0】返回主菜单')
  
#修改昵称回调函数集合
def entry_user_surnom_function(user, mes):
    global g_state
    g_state = state_list.secondary_user_surnom
    mcsql.add_location('3',now_username)#数据库记录
def user_surnom_process_function(user, mes):
    msg_content = mes
    tryprint(user, "已修改你的昵称为【%s】,自动返回主菜单" % msg_content)
    mcsql.add_userdetail('surnom',msg_content,now_username)#数据库记录
    global g_state
    g_state = state_list.main
    mcsql.add_location('0',now_username)#数据库记录
        
#位置信息回调函数集合
def entry_city_function(user, mes):
    global g_state
    g_state = state_list.secondary_city
    mcsql.add_location('4',now_username)#数据库记录
def city_process_function(user, mes):
    msg_content = mes
    argvs = sys.argv
    location = argvs[1] if len(argvs) >= 2 else msg_content
    weather_api_return = weatherreturn.futureWeather(location)
    if weather_api_return == '0':
        wx_inst.send_text(user, "请输入正确城市名称")
    else:
        wx_inst.send_text(user, "已修改所在城市为【%s】,自动返回主菜单" % msg_content)
        mcsql.add_weather_city(location,now_username)#数据库记录
        global g_state
        g_state = state_list.main
        mcsql.add_location('0',now_username)#数据库记录
        
#个人星座记录回调函数集合
def entry_user_signs_function(user, mes):
    global g_state
    g_state = state_list.secondary_user_signs
    mcsql.add_location('5',now_username)#数据库记录
def user_signs_process_function(user, mes):
    msg_content = mes
    fortune_api_return = fortuneReturn.get_fortune_star(msg_content)
    if fortune_api_return == '0':
        wx_inst.send_text(user, "请输入正确格式的星座名称(xx座)")
    else:
        wx_inst.send_text(user, "已修改你的星座为【%s】,自动返回主菜单" % msg_content)
        mcsql.add_userdetail('signs',msg_content,now_username)#数据库记录
        global g_state
        g_state = state_list.main
        mcsql.add_location('0',now_username)#数据库记录
        
#个人生日记录回调函数集合
def entry_user_birthday_function(user, mes):
    global g_state
    g_state = state_list.secondary_user_birthday
    mcsql.add_location('43',now_username)#数据库记录
def user_birthday_process_function(user, mes):
    msg_content = mes
    msg_content_date = msg_content.replace(' ','')
    if len(msg_content_date) != 4 :
        wx_inst.send_text(user, "请按照举例格式来添加生日信息,如【0101】")
    else:
        try:
            msg_content_date_int = int(msg_content_date)
        except ValueError:
            wx_inst.send_text(user, "请按照举例格式来添加生日信息,如【0101】")
        else:
            wx_inst.send_text(user, "已修改你的生日为【%s月%s日】,自动返回主菜单" % (msg_content_date[0:2],msg_content_date[2:4]))
            mcsql.add_userdetail('birthday',msg_content_date,now_username)#数据库记录
            global g_state
            g_state = state_list.main
            mcsql.add_location('0',now_username)#数据库记录
    
#历史菜单回调函数
def get_history_function(user, mes):
    wx_inst.send_text(user, historyReturn.set_history_date())
    wx_inst.send_text(user, "重新输入数字【5】可刷新内容,【0】返回主菜单")
 
    
#新闻菜单回调函数集合  
def entry_news_function(user, mes):
    global g_state
    g_state = state_list.secondary_news
    mcsql.add_location('6',now_username)#数据库记录
    
def news_process_function(user, mes):
    msg_content = mes
    
    if msg_content == '1' :
         tryprint(user,'%s' % newsReturn.get_news_from_type('top'))
         wx_inst.send_text(user, "重新输入【1】可刷新内容,【0】返回主菜单")
    elif msg_content == '2' :
         wx_inst.send_text(user, "最新社会新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('shehui'))
    elif msg_content == '3' :
         wx_inst.send_text(user, "最新国内新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('guonei'))
    elif msg_content == '4' :
         wx_inst.send_text(user, "最新国际新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('guoji'))
    elif msg_content == '5' :
         wx_inst.send_text(user, "最新娱乐新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('yule'))
    elif msg_content == '6' :
         wx_inst.send_text(user, "最新体育新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('tiyu'))
    elif msg_content == '7' :
         wx_inst.send_text(user, "最新军事新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('junshi'))
    elif msg_content == '8' :
         wx_inst.send_text(user, "最新科技新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('keji'))
    elif msg_content == '9' :
         wx_inst.send_text(user, "最新财经新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('caijing'))
    elif msg_content == '10' :
         wx_inst.send_text(user, "最新时尚新闻:")
         tryprint(user,'%s' % newsReturn.get_news_from_type('shishang'))
    else:
        wx_inst.send_text(user,'%s' % "请输入新闻类型对应的数字查询喔,【0】返回主菜单")
                     
    return True      


#黄历菜单回调函数集合        
def get_almanac_function(user, mes):
    tryprint(user, almanacReturn.set_almanac_date())    


#星座回调函数集合
def entry_fortune_function(user, mes):
    global g_state
    g_state = state_list.secondary_fortune
    mcsql.add_location('8',now_username)#数据库记录
    
def fortune_process_function(user, mes):
    msg_content = mes
    
    if msg_content == '1' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('白羊座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '2' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('金牛座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '3' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('双子座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '4' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('巨蟹座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '5' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('狮子座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '6' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('处女座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '7' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('天秤座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '8' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('天蝎座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '9' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('射手座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '10' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('摩羯座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '11' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('水瓶座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    elif msg_content == '12' :
         tryprint(user,'%s' % fortuneReturn.get_fortune_star('双鱼座'))
         wx_inst.send_text(user,'%s' % "输入星座对应的数字继续查询,【0】返回主菜单")
    else:
        wx_inst.send_text(user,'%s' % "请输入星座对应的数字查询喔,【0】返回主菜单")
                     
    return True      

#房源回调函数    
  
#热搜回调函数
def get_topsearch_function(user,mes):
    wx_inst.send_text(user, "请稍等,正在为您查询...(大约需要5秒钟)")
    tryprint(user,topsearchhttp.topsearch())
    
#生日爱心回调函数  
def get_heart_function(user,mes):
    wx_inst.send_text(user, "今天是麻麻的生日!")
    time.sleep(1)
    wx_inst.send_text(user, "生日快乐")
    time.sleep(1)
    wx_inst.send_img(user,'C:\\python\\heart.png')
    
#个人信息回调函数
def get_userdetail_function(user, mes):
    user_surname = mcsql.show_userdetail('surnom',now_username)
    user_city = mcsql.show_userdetail('weather_city',now_username)
    user_signs = mcsql.show_userdetail('signs',now_username)
    user_birthday = mcsql.show_userdetail('birthday',now_username)
    wx_inst.send_text(user, "~~完善个人信息可以更好的使用订阅功能~~\n以下是你的个人信息:\n昵称:%s\n城市:%s\n星座:%s\n生日:%s月%s日\n\n发送【昵称】更改昵称\n发送【城市】更改城市\n发送【星座】更改星座\n发送【生日】添加生日\n不需要方框,只要发送【】内关键词就可进行修改\n【0】返回主菜单" % (user_surname,user_city,user_signs,user_birthday[0:2],user_birthday[2:4]) )
 
#广播回调函数集合
def entry_broadcast_function(user, mes):
    global g_state
    g_state = state_list.secondary_broadcast
    mcsql.add_location('946743',now_username)#数据库记录
    
def broadcast_process_function(user, mes):
    msg_content = mes
    #检查是不是我
    if user != 'Zhouxinyue1401965853':
        pass
    else:
        #获取所有用户名称
        users = mcsql.show_wechat_name()
        for user in users:
            wx_inst.send_text(user,msg_content)
        global g_state
        g_state = state_list.main
        mcsql.add_location('0',now_username)#数据库记录

#热榜菜单回调函数集合  
def entry_tophub_function(user, mes):
    global g_state
    g_state = state_list.secondary_tophub
    mcsql.add_location('9',now_username)#数据库记录
    
def tophub_process_function(user, mes):
    msg_content = mes
    rere = '发送感兴趣的标题名称为你搜索详细内容,输入【】内对应数字继续查询,输入其他数字再现查询目录,【0】返回主菜单'
    if msg_content == '1' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('Jb0vmloB1G'))
         wx_inst.send_text(user, rere)
    elif msg_content == '2' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('x9ozB4KoXb'))
         wx_inst.send_text(user, rere)
    elif msg_content == '3' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('74Kvx59dkx'))
         wx_inst.send_text(user, rere)
    elif msg_content == '4' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('Y3QeLGAd7k'))
         wx_inst.send_text(user, rere)
    elif msg_content == '5' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('n4qv90roaK'))
         wx_inst.send_text(user, rere)
    elif msg_content == '6' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('3QeLGVPd7k'))
         wx_inst.send_text(user, rere)
    elif msg_content == '7' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('rx9ozj7oXb'))
         wx_inst.send_text(user, rere)
    elif msg_content == '8' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('G2me3ndwjq'))
         wx_inst.send_text(user, rere)
    elif msg_content == '9' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('mDOvnyBoEB'))
         wx_inst.send_text(user, rere)
    elif msg_content == '10' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('nBe0JLBv37'))
         wx_inst.send_text(user, rere)
    elif msg_content == '11' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('m4ejbjyexE'))
         wx_inst.send_text(user, rere)
    elif msg_content == '12' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('anoppbRolZ'))
         wx_inst.send_text(user, rere)
    elif msg_content == '13' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('NaEdZRMdrO'))
         wx_inst.send_text(user, rere)
    elif msg_content == '14' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('Y3QeLzPv7k'))
         wx_inst.send_text(user, rere)
    elif msg_content == '15' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('YqoXQR0vOD'))
         wx_inst.send_text(user, rere)
    elif msg_content == '16' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('wWmoOVYe4E'))
         wx_inst.send_text(user, rere)
    elif msg_content == '17' :
         tryprint(user,'%s' % tophubReturn.get_tophub_name('Ywv4jRxePa'))
         wx_inst.send_text(user, rere)
    else:
        try:
            msg_content_int = int(msg_content)
        except ValueError:
            target_url = "https://www.baidu.com/s?wd=%s"% msg_content
            wx_inst.send_link_card(
                to_user = user,
                title = msg_content,
                desc="点我,点我,查看搜索结果",
                target_url = target_url,
                img_url='https://i.loli.net/2021/01/14/x7nFUs5H698qlrt.jpg'
                )
            wx_inst.send_text(user, "输入【】内对应数字继续查询,输入其他数字再现查询目录,【0】返回主菜单")
        else:
            wx_inst.send_text(user, '''【热榜资讯】
高效获取最新最热资讯,让你一秒读懂朋友圈!
输入【】内对应数字进入
热点类：
【1】百度：实时热点
【2】今日头条：头条热榜
科技类：
【3】IT之家：日榜
【4】好奇心日报：大公司头条top15
【5】威锋网：今日新信息
财经类：
【6】财新网：评论热榜
【7】新浪财经新闻：点击量排行
【8】华尔街见闻：日排行
影视书籍类：
【9】电影新片榜
【10】热门剧集榜
【11】在映电影
【12】微信读书：飙升榜
娱乐类：
【13】网易娱乐：点击榜
【14】新浪：娱乐榜
游戏类：
【15】3DM游戏网：最新新闻
【16】机核网：每日最新
体育类：
【17】新浪：体育榜
【0】返回主菜单
''')
                     
    return True 

#我微信二维码回调函数  
def get_mywechat_function(user,mes):
    wx_inst.send_img(user,'C:\\python\\mywechat.jpg')
    
#文案回调函数集合   
def get_paperwork_function(user, mes):
    tryprint(user,paperworkReturn.get_paperwork_all())

    
#调休回调函数集合   
def get_holiday_function(user, mes):
    tryprint(user, holidayReturn.get_holiday_date())
    
#个人订阅回调函数 -------------------------------------------------------------------------------------------------------------------    
def get_subscription_function(user, mes):
    subscribed_list = []
    unsubscribed_list = []
    #列表添加通用函数
    def add_function(switch,funcname,time):
        switch_tag = mcsql.show_subs_user_switch(switch,now_username)
        if switch_tag == '1':
            time_tag = mcsql.show_subs_usetime(time,now_username)
            strtime = time_tag[0:2] +':'+ time_tag[2:4]
            func_to = funcname+'(发送时间'+strtime+')'
            subscribed_list.append(func_to)
        else:
            unsubscribed_list.append(funcname)   
    #查询数据库该用户是否订阅彩票
    add_function('subs_lottery_switch','【22001】每日彩票开奖信息','subs_lottery_time')
    #查询数据库该用户是否订阅天气
    add_function('subs_weather_switch','【22002】本地天气预报','subs_weather_time')
    #查询数据库该用户是否订阅睡觉
    add_function('subs_sleep_switch','【22003】提醒睡觉功能','subs_sleep_time')
    #查询数据库该用户是否订阅黄历
    add_function('subs_almanac_switch','【22004】黄历','subs_almanac_time')
    #查询数据库该用户是否订阅星座
    add_function('subs_fortune_switch','【22005】星座运势','subs_fortune_time')
    #查询数据库该用户是否订阅热点
    add_function('subs_topbaidu_switch','【22006】热点号角','subs_topbaidu_time')
    #查询数据库该用户是否订阅舔狗
    add_function('subs_tiangou_switch','【22007】舔狗日记','subs_tiangou_time')
    #整理输出
    subscribed = '\n'.join(subscribed_list)
    unsubscribed = '\n'.join(unsubscribed_list)
    wx_inst.send_text(user, "订阅功能可以在指定的时间为你发送内容，首次使用前,请先输入【01】填写个人信息,以获得最佳使用体验.如已填写完毕,请根据以下提示进行操作\n\n~~我的订阅~~\n已订阅功能:\n%s\n\n可订阅功能:\n%s\n\n【】内是功能编号，可发送编号来订阅功能/修改时间/取消订阅。\n【0】返回主菜单"%(subscribed,unsubscribed))

#订阅彩票回调函数集合
def entry_subs_lottery_function(user, mes):
    global g_state
    g_state = state_list.secondary_subs_lottery
    mcsql.add_location('22001',now_username)#数据库记录
    
#订阅天气回调函数集合
def entry_subs_weather_function(user, mes):
    global g_state
    g_state = state_list.secondary_subs_weather
    mcsql.add_location('22002',now_username)#数据库记录
    
#订阅睡觉回调函数集合
def entry_subs_sleep_function(user, mes):
    global g_state
    g_state = state_list.secondary_subs_sleep
    mcsql.add_location('22003',now_username)#数据库记录

#订阅黄历回调函数集合
def entry_subs_almanac_function(user, mes):
    global g_state
    g_state = state_list.secondary_subs_almanac
    mcsql.add_location('22004',now_username)#数据库记录

#订阅星座回调函数集合
def entry_subs_fortune_function(user, mes):
    global g_state
    g_state = state_list.secondary_subs_fortune
    mcsql.add_location('22005',now_username)#数据库记录
    
#订阅热点回调函数集合
def entry_subs_topbaidu_function(user, mes):
    global g_state
    g_state = state_list.secondary_subs_topbaidu
    mcsql.add_location('22006',now_username)#数据库记录
    
#订阅舔狗回调函数集合
def entry_subs_tiangou_function(user, mes):
    global g_state
    g_state = state_list.secondary_subs_tiangou
    mcsql.add_location('22007',now_username)#数据库记录
    
#通用更改订阅函数
def subs_all_function(user, mes,switch,substime):
    msg_content = mes
    global g_state
    if msg_content == '1' :
        mcsql.add_subs_switch(switch,'1',now_username)
        mcsql.add_subs_switch(substime,'0800',now_username)
        wx_inst.send_text(user, "已完成订阅,默认发送时间为早上8点,输入【2】修改发送时间,输入【0】保存设定,返回主菜单")
    if msg_content == '2' :
        wx_inst.send_text(user, "请按照举例格式来修改时间,如想修改为9点05分,则发送【0905】(24小时制)")
    if msg_content == '3' :
        mcsql.add_subs_switch(switch,'0',now_username)
        wx_inst.send_text(user, "已为你取消订阅,自动返回主菜单，发送【02】查看修改结果")
        g_state = state_list.main
        mcsql.add_location('0',now_username)#数据库记录
    if len(msg_content) == 4 :
        try:
            msg_content_int = int(msg_content)
        except ValueError:
            wx_inst.send_text(user, "请按照举例格式来修改时间,如想修改为9点05分,则发送【0905】(24小时制)")
        else:
            wx_inst.send_text(user, "已修改发送时间为每天【%s:%s】,自动返回主菜单,发送【02】查看修改结果" % (msg_content[0:2],msg_content[2:4]))
            mcsql.add_subs_switch(substime,msg_content,now_username)#数据库记录
            g_state = state_list.main
            mcsql.add_location('0',now_username)#数据库记录

        
main_wechat_robot = function_object.Function_main("1", main_dict, entry_robot_function)
main_choice_query = function_object.Function_main("2", main_dict, entry_choice_function)
main_lottery_query = function_object.Function_main("3", main_dict, get_lottery_function)
main_weather_query = function_object.Function_main("4",main_dict,entry_weather_function)
main_history_query = function_object.Function_main("5", main_dict, get_history_function)
main_news_query = function_object.Function_main("6", main_dict, entry_news_function)
main_almanac_query = function_object.Function_main("7", main_dict, get_almanac_function)
main_fortune_query = function_object.Function_main("8", main_dict, entry_fortune_function)
main_topsearch_query = function_object.Function_main("热搜",main_dict,get_topsearch_function)
main_heart_query = function_object.Function_main("今天是什么日子110110",main_dict,get_heart_function)
main_city_query = function_object.Function_main("城市",main_dict,entry_city_function)
main_userdetail_query = function_object.Function_main("01", main_dict, get_userdetail_function)
main_user_signs_query = function_object.Function_main("星座",main_dict,entry_user_signs_function)
main_user_surnom_query = function_object.Function_main("昵称",main_dict,entry_user_surnom_function)
main_user_birthday_query = function_object.Function_main("生日",main_dict,entry_user_birthday_function)
main_subscription_query = function_object.Function_main("02", main_dict, get_subscription_function)
main_broadcast_query = function_object.Function_main("946743", main_dict, entry_broadcast_function)
main_tophub_query = function_object.Function_main("9", main_dict, entry_tophub_function)
main_mywechat_query = function_object.Function_main("666",main_dict,get_mywechat_function)
main_paperwork_query = function_object.Function_main("10", main_dict, get_paperwork_function)
main_holiday_query = function_object.Function_main("11", main_dict, get_holiday_function)
#订阅功能
main_subs_lottery_query = function_object.Function_main("22001", main_dict, entry_subs_lottery_function)
main_subs_weather_query = function_object.Function_main("22002", main_dict, entry_subs_weather_function)
main_subs_sleep_query = function_object.Function_main("22003", main_dict, entry_subs_sleep_function)
main_subs_almanac_query = function_object.Function_main("22004", main_dict, entry_subs_almanac_function)
main_subs_fortune_query = function_object.Function_main("22005", main_dict, entry_subs_fortune_function)
main_subs_topbaidu_query = function_object.Function_main("22006", main_dict, entry_subs_topbaidu_function)
main_subs_tiangou_query = function_object.Function_main("22007", main_dict, entry_subs_tiangou_function)


main_dict_class = {
    "1": main_wechat_robot, 
    "2": main_choice_query, 
    "3": main_lottery_query, 
    "4": main_weather_query, 
    "5": main_history_query,
    "6": main_news_query, 
    "7": main_almanac_query,
    "8": main_fortune_query,
    "热搜": main_topsearch_query,
    "今天是什么日子110110": main_heart_query,
    "城市": main_city_query,
    "01": main_userdetail_query,
    "星座": main_user_signs_query,
    "昵称": main_user_surnom_query,
    "生日": main_user_birthday_query,
    "02": main_subscription_query,
    "946743": main_broadcast_query,
    "9": main_tophub_query, 
    "666": main_mywechat_query,
    "10": main_paperwork_query,
    "11": main_holiday_query,
    #订阅
    "22001": main_subs_lottery_query,
    "22002": main_subs_weather_query,
    "22003": main_subs_sleep_query,
    "22004": main_subs_almanac_query,
    "22005": main_subs_fortune_query,
    "22006": main_subs_topbaidu_query,
    "22007": main_subs_tiangou_query,
}


#机器人二级菜单返回字符串，二级菜单对象及对象集合
secondary_robot_dict = {
'0':'''【停止机器人对话，已返回主菜单】
''' + menustr,

}

secondary_robot_chat = function_object.Function_main("0", secondary_robot_dict,exit_all_function)
secondary_robot_custom = function_object.Function_main("欣悦", secondary_robot_dict, custom_robot_function)

robot_dict_class = {
    "0": secondary_robot_chat, 
    "欣悦": secondary_robot_custom, 
}


#选择二级菜单返回字符串，二级菜单对象及对象集合
secondary_choice_dict = {
'0':'''【停止选择功能，已返回主菜单】
''' + menustr,
}

secondary_choice_chat = function_object.Function_main("0", secondary_choice_dict, exit_all_function)


choice_dict_class = {
    "0": secondary_choice_chat, 
}

#天气二级菜单返回字符串，二级菜单对象及对象集合
secondary_weather_dict = {
'0':'''【结束天气查询，已返回主菜单】
''' + menustr,
}

secondary_weather_chat = function_object.Function_main("0", secondary_weather_dict, exit_all_function)


weather_dict_class = {
    "0": secondary_weather_chat, 
}

#昵称二级菜单返回字符串，二级菜单对象及对象集合
secondary_user_surnom_dict = {
'0':'''【结束昵称的修改，已返回主菜单】
''' + menustr,
}

secondary_user_surnom_chat = function_object.Function_main("0", secondary_user_surnom_dict, exit_all_function)


user_surnom_dict_class = {
    "0": secondary_user_surnom_chat, 
}

#城市二级菜单返回字符串，二级菜单对象及对象集合
secondary_city_dict = {
'0':'''【结束城市修改，已返回主菜单】
''' + menustr,
}

secondary_city_chat = function_object.Function_main("0", secondary_city_dict, exit_all_function)


city_dict_class = {
    "0": secondary_city_chat, 
}

#个人星座二级菜单返回字符串，二级菜单对象及对象集合
secondary_user_signs_dict = {
'0':'''【结束个人星座的修改，已返回主菜单】
''' + menustr,
}

secondary_user_signs_chat = function_object.Function_main("0", secondary_user_signs_dict, exit_all_function)


user_signs_dict_class = {
    "0": secondary_user_signs_chat, 
}

#生日二级菜单返回字符串，二级菜单对象及对象集合
secondary_user_birthday_dict = {
'0':'''【结束生日日期的修改，已返回主菜单】
''' + menustr,
}

secondary_user_birthday_chat = function_object.Function_main("0", secondary_user_birthday_dict, exit_all_function)


user_birthday_dict_class = {
    "0": secondary_user_birthday_chat, 
}

#新闻二级菜单返回字符串，二级菜单对象及对象集合
secondary_news_dict = {
'0':'''【停止新闻浏览，已返回主菜单】
''' + menustr,
}

secondary_news_chat = function_object.Function_main("0", secondary_news_dict, exit_all_function)


news_dict_class = {
    "0": secondary_news_chat, 
}


#星座二级菜单返回字符串，二级菜单对象及对象集合
secondary_fortune_dict = {
'0':'''【停止星座查询，已返回主菜单】
''' + menustr,
}

secondary_fortune_chat = function_object.Function_main("0", secondary_fortune_dict, exit_all_function)


fortune_dict_class = {
    "0": secondary_fortune_chat, 
}

#广播二级菜单返回字符串，二级菜单对象及对象集合
secondary_broadcast_dict = {
'0':'''【停止广播，已返回主菜单】
''' + menustr,
}

secondary_broadcast_chat = function_object.Function_main("0", secondary_broadcast_dict, exit_all_function)


broadcast_dict_class = {
    "0": secondary_broadcast_chat, 
}

#热点二级菜单返回字符串，二级菜单对象及对象集合
secondary_tophub_dict = {
'0':'''【停止热点浏览，已返回主菜单】
''' + menustr,
}

secondary_tophub_chat = function_object.Function_main("0", secondary_tophub_dict, exit_all_function)


tophub_dict_class = {
    "0": secondary_tophub_chat, 
}

#订阅彩票二级菜单回字符串，二级菜单对象及对象集合---------------------------------------------------------------------------
secondary_subs_lottery_dict = {
'0':'''【停止此功能的订阅修改，已返回主菜单】
''' + menustr,
}

secondary_subs_lottery_chat = function_object.Function_main("0", secondary_subs_lottery_dict, exit_all_function)


subs_lottery_dict_class = {
    "0": secondary_subs_lottery_chat, 
}

#订阅天气二级菜单回字符串，二级菜单对象及对象集合
secondary_subs_weather_dict = {
'0':'''【停止此功能的订阅修改，已返回主菜单】
''' + menustr,
}

secondary_subs_weather_chat = function_object.Function_main("0", secondary_subs_weather_dict, exit_all_function)


subs_weather_dict_class = {
    "0": secondary_subs_weather_chat, 
}

#订阅睡觉二级菜单回字符串，二级菜单对象及对象集合
secondary_subs_sleep_dict = {
'0':'''【停止此功能的订阅修改，已返回主菜单】
''' + menustr,
}

secondary_subs_sleep_chat = function_object.Function_main("0", secondary_subs_sleep_dict, exit_all_function)


subs_sleep_dict_class = {
    "0": secondary_subs_sleep_chat, 
}

#订阅黄历二级菜单回字符串，二级菜单对象及对象集合
secondary_subs_almanac_dict = {
'0':'''【停止此功能的订阅修改，已返回主菜单】
''' + menustr,
}

secondary_subs_almanac_chat = function_object.Function_main("0", secondary_subs_almanac_dict, exit_all_function)


subs_almanac_dict_class = {
    "0": secondary_subs_almanac_chat, 
}

#订阅星座二级菜单回字符串，二级菜单对象及对象集合
secondary_subs_fortune_dict = {
'0':'''【停止此功能的订阅修改，已返回主菜单】
''' + menustr,
}

secondary_subs_fortune_chat = function_object.Function_main("0", secondary_subs_fortune_dict, exit_all_function)


subs_fortune_dict_class = {
    "0": secondary_subs_fortune_chat, 
}

#订阅热点二级菜单回字符串，二级菜单对象及对象集合
secondary_subs_topbaidu_dict = {
'0':'''【停止此功能的订阅修改，已返回主菜单】
''' + menustr,
}

secondary_subs_topbaidu_chat = function_object.Function_main("0", secondary_subs_topbaidu_dict, exit_all_function)


subs_topbaidu_dict_class = {
    "0": secondary_subs_topbaidu_chat, 
}

#订阅舔狗二级菜单回字符串，二级菜单对象及对象集合
secondary_subs_tiangou_dict = {
'0':'''【停止此功能的订阅修改，已返回主菜单】
''' + menustr,
}

secondary_subs_tiangou_chat = function_object.Function_main("0", secondary_subs_tiangou_dict, exit_all_function)


subs_tiangou_dict_class = {
    "0": secondary_subs_tiangou_chat, 
}

# 消息处理回调函数========================================================================================================
def thread_handle_message(wx_inst):
    #死循环，一直运行
    global tuling_switch
    global g_frist_entry_flag
    global g_state
    global now_username
    global mcsql
    while True:
        #堵塞等待消息，有了就读出来
        msg_temp = queue_recved_message.get()
        print('显示接收的信息')
        print(msg_temp)
        if 'msg' in msg_temp.get('type'):
            # 这里是判断收到的是消息 不是别的响应
            msg_content = msg_temp.get('data', {}).get('msg', '')  #获取消息内容
            send_or_recv = msg_temp.get('data', {}).get('send_or_recv', '') #收还是发
            wechar_user = msg_temp.get('data', {}).get('from_wxid','') #消息是谁发的
            if send_or_recv[0] == '0':
                #如果收到消息则走以下处理
                if len(msg_content)>30:
                    pass
                else:
#----------------------------------------------------------------------------------------------------------------
#用户信息位置数据库标记
                    now_username = '%s' % msg_temp['data'].get('from_wxid') #获取用户id的str
                    print('显示用户名称')
                    print(now_username)
                    #查询用户名是否存在于数据库
                    mcsql = MCsql()
                    user_names_list = mcsql.show_wechat_name()
                    if now_username in user_names_list:#如果存在则跳过
                        g_frist_entry_flag = False
                        pass
                    if now_username not in user_names_list:#如果不存在则将微信名存入数据库,默认初始在主菜单
                        mcsql.add_wechat_name(now_username)
                        #第一条不论内容是什么都发主菜单
                        g_frist_entry_flag = True
                        pass
                    #查询用户位置,改变全局位置
                    if mcsql.show_location(now_username) == '0':
                        g_state = state_list.main
          
                    elif mcsql.show_location(now_username) == '1':
                        g_state = state_list.secondary_robot
              
                    elif mcsql.show_location(now_username) == '2':
                        g_state = state_list.secondary_choice
            
                    elif mcsql.show_location(now_username) == '3':
                        g_state = state_list.secondary_user_surnom
                 
                    elif mcsql.show_location(now_username) == '4':
                        g_state = state_list.secondary_city
                        
                    elif mcsql.show_location(now_username) == '44':
                        g_state = state_list.secondary_weather
                        
                    elif mcsql.show_location(now_username) == '43':
                        g_state = state_list.secondary_user_birthday
                        
                    elif mcsql.show_location(now_username) == '5':
                        g_state = state_list.secondary_user_signs
                        
                    elif mcsql.show_location(now_username) == '6':
                        g_state = state_list.secondary_news
                        
                    elif mcsql.show_location(now_username) == '8':
                        g_state = state_list.secondary_fortune
                        
                    elif mcsql.show_location(now_username) == '946743':
                        g_state = state_list.secondary_broadcast
                        
                    elif mcsql.show_location(now_username) == '9':
                        g_state = state_list.secondary_tophub
                        
                    elif mcsql.show_location(now_username) == '22001':
                        g_state = state_list.secondary_subs_lottery
                        
                    elif mcsql.show_location(now_username) == '22002':
                        g_state = state_list.secondary_subs_weather
                        
                    elif mcsql.show_location(now_username) == '22003':
                        g_state = state_list.secondary_subs_sleep
                        
                    elif mcsql.show_location(now_username) == '22004':
                        g_state = state_list.secondary_subs_almanac
                        
                    elif mcsql.show_location(now_username) == '22005':
                        g_state = state_list.secondary_subs_fortune
                        
                    elif mcsql.show_location(now_username) == '22006':
                        g_state = state_list.secondary_subs_topbaidu
                        
                    elif mcsql.show_location(now_username) == '22007':
                        g_state = state_list.secondary_subs_tiangou
                        
                    if g_frist_entry_flag == True:  #如果是第一次进入，则发送介绍信息
                        wx_inst.send_text(wechar_user, '初次见面你好呀~欢迎使用小欣悦机器人!\n以下是我的使用介绍')
                        wx_inst.send_text(wechar_user, main_dict['0']) #发送介绍信息
                    else:  #如果不是第一次进入，则根据具体菜单内容进行回复
    #-----------------------------------------------------------------------------------------------------------------
    #通过菜单指针进入菜单              
                        if g_state == state_list.main: #如果是在主菜单，则根据关键字遍历主菜单服务
                            temp = function_object.find_value_from_dict(msg_content, main_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                                    
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                temp.process_call_back()
                            else:
                                wx_inst.send_text(wechar_user, main_dict['0'])
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                                
                        elif g_state == state_list.secondary_robot: #如果是机器人菜单，则遍历机器人菜单
                            temp = function_object.find_value_from_dict(msg_content, robot_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                robot_process_function(wechar_user, msg_content) #如果无关键字则到图灵机器人交互
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                        
                        elif g_state == state_list.secondary_choice: #如果是选择菜单，则遍历选择菜单
                            temp = function_object.find_value_from_dict(msg_content, choice_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                choice_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))   
                            
                        elif g_state == state_list.secondary_weather: #如果是天气菜单，则遍历选择菜单
                            temp = function_object.find_value_from_dict(msg_content, weather_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                weather_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))  
                            
                        elif g_state == state_list.secondary_user_surnom: #如果是昵称菜单，则遍历选择菜单
                            temp = function_object.find_value_from_dict(msg_content, user_surnom_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                user_surnom_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))  
                            
                        elif g_state == state_list.secondary_city: #如果是城市菜单，则遍历选择菜单
                            temp = function_object.find_value_from_dict(msg_content, city_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                city_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))   
                            
                            
                        elif g_state == state_list.secondary_user_signs: #如果是个人星座菜单，则遍历选择菜单
                            temp = function_object.find_value_from_dict(msg_content, user_signs_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                user_signs_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))  
                            
                        elif g_state == state_list.secondary_user_birthday: #如果是生日菜单，则遍历选择菜单
                            temp = function_object.find_value_from_dict(msg_content, user_birthday_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                user_birthday_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))  
                            
                                
                        elif g_state == state_list.secondary_news: #如果是新闻菜单，则遍历新闻菜单
                            temp = function_object.find_value_from_dict(msg_content, news_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                news_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                                
                        elif g_state == state_list.secondary_fortune: #如果是星座菜单，则遍历星座菜单
                            temp = function_object.find_value_from_dict(msg_content, fortune_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                fortune_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                            
                        elif g_state == state_list.secondary_broadcast: #如果是广播菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, broadcast_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                broadcast_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
    
                        elif g_state == state_list.secondary_tophub: #如果是热点菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, tophub_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                tophub_process_function(wechar_user, msg_content) #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
    #------------------------------------------------------------------------------------------------------------------------------------------------
                        elif g_state == state_list.secondary_subs_lottery: #如果是订阅彩票菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, subs_lottery_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                subs_all_function(wechar_user, msg_content,'subs_lottery_switch','subs_lottery_time') #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                            
                        elif g_state == state_list.secondary_subs_weather: #如果是订阅天气菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, subs_weather_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                subs_all_function(wechar_user, msg_content,'subs_weather_switch','subs_weather_time') #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                            
                        elif g_state == state_list.secondary_subs_sleep: #如果是订阅睡觉菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, subs_sleep_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                subs_all_function(wechar_user, msg_content,'subs_sleep_switch','subs_sleep_time') #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                            
                        elif g_state == state_list.secondary_subs_almanac: #如果是订阅黄历菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, subs_almanac_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                subs_all_function(wechar_user, msg_content,'subs_almanac_switch','subs_almanac_time') #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                            
                        elif g_state == state_list.secondary_subs_fortune: #如果是订阅星座菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, subs_fortune_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                subs_all_function(wechar_user, msg_content,'subs_fortune_switch','subs_fortune_time') #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                            
                        elif g_state == state_list.secondary_subs_topbaidu: #如果是订阅热点菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, subs_topbaidu_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                subs_all_function(wechar_user, msg_content,'subs_topbaidu_switch','subs_topbaidu_time') #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                            
                        elif g_state == state_list.secondary_subs_tiangou: #如果是订阅舔狗菜单，则遍历
                            temp = function_object.find_value_from_dict(msg_content, subs_tiangou_dict_class) #得到对象
                            if temp != None:  #如果关键字能找到结构体，代表是设置的指令
                                temp.wx_message = msg_content
                                temp.wx_user = wechar_user
                                if temp.print_message != None: #如果对象中有要反馈的微信消息则返回
                                    wx_inst.send_text(wechar_user, temp.print_message)
                        
                                temp.process_call_back()   
                            else:
                                subs_all_function(wechar_user, msg_content,'subs_tiangou_switch','subs_tiangou_time') #如果不是关键字,则运行此函数
                            print('收到消息后变更的gstate=%s,id=%s'% (g_state,now_username))
                                
                    # 0是收到的消息 1是发出的 对于1不要再回复了 不然会无限循环回复
                    #wx_inst.send_text('filehelper', '收到消息:{}'.format(msg_content))

# 时间处理回调函数========================================================================================================$
def job():
    global g_state
    #获取当前时间
    now_ti = datetime.datetime.now()
    now_time = now_ti.strftime("%H%M")
    now_time_plus = datetime.datetime.now() + datetime.timedelta(hours=1)
    now_time_plus_one = now_time_plus.strftime("%H%M")
    mcsql = MCsql()
    
    
    #数据库查询是否有彩票订阅,如果有就出用户列表
    subs_lottery_switch = mcsql.show_subs_switch('subs_lottery_switch')
    all_lottery_switch = subs_lottery_switch.find('1')
    if all_lottery_switch == -1:  
        #如果没有人订阅pass
        pass
    else:  
        #如果有人订阅,获取用户名列表
        subs_lottery_users = mcsql.show_subs_users('subs_lottery_switch')
        #逐一查询数据库时间是否和当前时间相同
        for subs_user in subs_lottery_users:
            subs_usetime = mcsql.show_subs_usetime('subs_lottery_time',subs_user)
            if subs_usetime != now_time:
                #如果时间不一致pass
                pass
            else:
                #如果时间一致,返回订阅内容
                surname = mcsql.show_userdetail('surnom',subs_user)
                wx_inst.send_text(subs_user, '%s,这是你订阅的每日彩票开奖信息,小欣悦为你添好运!'%surname)
                wx_inst.send_text(subs_user, lotteryReturn.lottery_all())
                
    #数据库查询是否有天气订阅,如果有就出用户列表
    subs_weather_switch = mcsql.show_subs_switch('subs_weather_switch')
    all_weather_switch = subs_weather_switch.find('1')
    if all_weather_switch == -1:  
        #如果没有人订阅pass
        pass
    else:  
        #如果有人订阅,获取用户名列表
        subs_weather_users = mcsql.show_subs_users('subs_weather_switch')
        #逐一查询数据库时间是否和当前时间相同
        for subs_user in subs_weather_users:
            subs_usetime = mcsql.show_subs_usetime('subs_weather_time',subs_user)
            if subs_usetime != now_time:
                #如果时间不一致pass
                pass
            else:
                #如果时间一致,返回订阅内容
                surname = mcsql.show_userdetail('surnom',subs_user)
                wx_inst.send_text(subs_user, '%s~这是订阅的天气预报,祝你一天好心情'%surname)
                weather_city = mcsql.show_weather_city(subs_user)
                tryprint(subs_user, weatherreturn.fetchWeather(weather_city))
                tryprint(subs_user, weatherreturn.futureWeather(weather_city))
                
                
    #数据库查询是否有睡觉订阅,如果有就出用户列表
    subs_sleep_switch = mcsql.show_subs_switch('subs_sleep_switch')
    all_sleep_switch = subs_sleep_switch.find('1')
    if all_sleep_switch == -1:  
        #如果没有人订阅pass
        pass
    else:  
        #如果有人订阅,获取用户名列表
        subs_sleep_users = mcsql.show_subs_users('subs_sleep_switch')
        #逐一查询数据库时间是否和当前时间相同
        for subs_user in subs_sleep_users:
            subs_usetime = mcsql.show_subs_usetime('subs_sleep_time',subs_user)
            if subs_usetime == now_time_plus_one:
                #提前一小时催催睡觉
                surname = mcsql.show_userdetail('surnom',subs_user)
                wx_inst.send_text(subs_user, '小欣悦突然出现!【0】返回主菜单')
                sleep_before_list =['洗过澡了吗?手头上的事差不多可以准备收尾啦,让眼睛休息一下!','回家了没?','一会儿别吃夜宵了','今天我有没有派上用场呀?','这几天晚上睡得好吗?','不许不听话!']
                sleep_before = ''.join(random.sample(sleep_before_list, 1))
                wx_inst.send_text(subs_user, '%s,再过一小时就要睡觉了,%s'%(surname,sleep_before))
                g_state = state_list.secondary_robot
                mcsql.add_location('1',subs_user)#数据库记录
            elif subs_usetime == now_time:
                #如果时间一致,返回订阅内容
                surname = mcsql.show_userdetail('surnom',subs_user)
                sleep_word_list = ['被子盖盖好','身体好才是本钱','看完这条消息就把手机放下吧','被窝里多舒服啊','这个世界上有百分之五十的烦恼,都是通过好好睡一觉就能解决的,至于剩下的一半,等睡醒再去想','江湖不好混了,躲被窝里吧',]
                sleep_word = ''.join(random.sample(sleep_word_list, 1))
                wx_inst.send_text(subs_user, '睡觉时间到啦,%s~该休息了!%s,祝你好梦OwO'%(surname,sleep_word))
                tryprint(subs_user, sleepstoryReturn.get_bulletin()) 
                g_state = state_list.secondary_robot
                mcsql.add_location('1',subs_user)#数据库记录
            else:
                #如果时间不一致pass
                pass
                
    #数据库查询是否有黄历订阅,如果有就出用户列表
    subs_almanac_switch = mcsql.show_subs_switch('subs_almanac_switch')
    all_almanac_switch = subs_almanac_switch.find('1')
    if all_almanac_switch == -1:  
        #如果没有人订阅pass
        pass
    else:  
        #如果有人订阅,获取用户名列表
        subs_almanac_users = mcsql.show_subs_users('subs_almanac_switch')
        #逐一查询数据库时间是否和当前时间相同
        for subs_user in subs_almanac_users:
            subs_usetime = mcsql.show_subs_usetime('subs_almanac_time',subs_user)
            if subs_usetime != now_time:
                #如果时间不一致pass
                pass
            else:
                #如果时间一致,返回订阅内容
                surname = mcsql.show_userdetail('surnom',subs_user)
                wx_inst.send_text(subs_user, '我夜观天象,整理出了%s今日的行动指南'%surname)
                tryprint(subs_user, almanacReturn.set_almanac_date()) 
    
    #数据库查询是否有星座订阅,如果有就出用户列表
    subs_fortune_switch = mcsql.show_subs_switch('subs_fortune_switch')
    all_fortune_switch = subs_fortune_switch.find('1')
    if all_fortune_switch == -1:  
        #如果没有人订阅pass
        pass
    else:  
        #如果有人订阅,获取用户名列表
        subs_fortune_users = mcsql.show_subs_users('subs_fortune_switch')
        #逐一查询数据库时间是否和当前时间相同
        for subs_user in subs_fortune_users:
            subs_usetime = mcsql.show_subs_usetime('subs_fortune_time',subs_user)
            if subs_usetime != now_time:
                #如果时间不一致pass
                pass
            else:
                #如果时间一致,查看是否有星座
                signs = mcsql.show_userdetail('signs',subs_user)
                fortune_api_return = fortuneReturn.get_fortune_star(signs)
                if fortune_api_return == '0':
                    wx_inst.send_text(subs_user, "当前无法为你推送,请在个人信息中完善星座信息【0】打开主菜单")
                else:
                    surname = mcsql.show_userdetail('surnom',subs_user)
                    wx_inst.send_text(subs_user, '%s今朝阿老来赛额'%surname)
                    tryprint(subs_user,fortune_api_return)
    
    #数据库查询是否有热点订阅,如果有就出用户列表
    subs_topbaidu_switch = mcsql.show_subs_switch('subs_topbaidu_switch')
    all_topbaidu_switch = subs_topbaidu_switch.find('1')
    if all_topbaidu_switch == -1:  
        #如果没有人订阅pass
        pass
    else:  
        #如果有人订阅,获取用户名列表
        subs_topbaidu_users = mcsql.show_subs_users('subs_topbaidu_switch')
        #逐一查询数据库时间是否和当前时间相同
        for subs_user in subs_topbaidu_users:
            subs_usetime = mcsql.show_subs_usetime('subs_topbaidu_time',subs_user)
            if subs_usetime != now_time:
                #如果时间不一致pass
                pass
            else:
                #如果时间一致,返回订阅内容
                surname = mcsql.show_userdetail('surnom',subs_user)
                wx_inst.send_text(subs_user, '%s,这是今日热点,请过目~'%surname)
                tryprint(subs_user,'%s' % tophubReturn.get_tophub_name('Jb0vmloB1G')) 
                
    #数据库查询是否有舔狗订阅,如果有就出用户列表
    subs_tiangou_switch = mcsql.show_subs_switch('subs_tiangou_switch')
    all_tiangou_switch = subs_tiangou_switch.find('1')
    if all_tiangou_switch == -1:  
        #如果没有人订阅pass
        pass
    else:  
        #如果有人订阅,获取用户名列表
        subs_tiangou_users = mcsql.show_subs_users('subs_tiangou_switch')
        #逐一查询数据库时间是否和当前时间相同
        for subs_user in subs_tiangou_users:
            subs_usetime = mcsql.show_subs_usetime('subs_tiangou_time',subs_user)
            if subs_usetime != now_time:
                #如果时间不一致pass
                pass
            else:
                #如果时间一致,返回订阅内容
                tryprint(subs_user,'%s' % sleepstoryReturn.get_tiangou()) 

def thread_handle_time(wx_inst):
    def main_job():
        sched.add_job(job, max_instances=10, trigger=DateTrigger(), id="123")
    # 定义BlockingScheduler
    sched = BackgroundScheduler()
    sched.add_job(main_job, 'interval', seconds=60)
    sched.start()       
    tornado.ioloop.IOLoop.instance().start()
        
        
# wx_inst = WechatPCAPI(on_message=do_nothing, log=logging)
def main():
    #创建一个wechatpcapi对象，传递的参数为函数对象，类似回调函数。
    wx_inst.start_wechat(block=True)
    
    while not wx_inst.get_myself():
        time.sleep(5)

    print('登陆成功')
    
    #得到自己的信息
    print(wx_inst.get_myself())

    
    #创建一个现线程来一直运行，读取消息
    threading.Thread(target=thread_handle_message, args=(wx_inst,)).start()

    #创建一个现线程来一直运行，获取时间
    threading.Thread(target=thread_handle_time, args=(wx_inst,)).start()

if __name__ == '__main__':
    main()

