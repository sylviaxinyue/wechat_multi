# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 17:22:43 2020

@author: Administrator
"""

import requests
import datetime
import random

appkey = "a7584d259fc577693bdae3e99fc9511c"
month = ('%s' % datetime.datetime.now().month)
day = ('%s' % datetime.datetime.now().day)
date = ('%s/%s'%(month,day))

def main():
    print(set_history_date())

def set_history_date():
    url = "http://v.juhe.cn/todayOnhistory/queryEvent.php"
    params = {
        "date": date, #日期
        "key": appkey,  # 应用APPKEY(应用详细页查询)
        "dtype": "json",  # 返回数据的格式,xml或json，默认json
    }
 
    f = requests.get(url=url, params=params)
    res = f.json()
 
    if res:
        error_code = res["error_code"]
        if error_code == 0:
            # 成功请求
            w = res["result"]
            rets_list = []
            rws = random.sample(w, 10)
            for rw in rws:
                ret_tuple =('[%s]' % rw.get('date'),
                            '%s' % rw.get('title'),'\n'
                           )
                rets_list += ret_tuple
            
            rets_str = ''.join(rets_list)
            return rets_str
        else:
            return 0
        
if __name__ == '__main__':
    main()