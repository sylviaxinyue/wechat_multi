# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 13:26:57 2021

@author: Administrator
"""
from lxml import etree
import requests

#网址
baidu_hotspot = 'Jb0vmloB1G'
#科技
ithome_day_top = '74Kvx59dkx'
#财经
caixin_comments_top = '3QeLGVPd7k'
xinlang_finance = 'Ywv4jRxePa'
#影视
douban_new_film = 'mDOvnyBoEB'
douban_now_showing_film = 'm4ejbjyexE'
douban_hot_tvplay = 'nBe0JLBv37'
#游戏
game3dm_news = 'YqoXQR0vOD'
#体育
xinlang_sport = 'Ywv4jRxePa'

def get_tophub_name(uurl):
    url = 'https://tophub.today/n/%s'% uurl
    kv = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'}
    try:
        r = requests.get(url = url,headers = kv)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        html = etree.HTML(r.text)
        res = html.xpath('(//table//tbody)[1]//*[@class="al"]//a/text()')
        re = '\n- '.join(res)
        ret = '- ' + re
        return ret
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题'

# def get_tophub_name_href(uurl):
#     url = 'https://tophub.today/n/%s'% uurl
#     kv = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'}
#     try:
#         r = requests.get(url = url,headers = kv)
#         r.raise_for_status()
#         r.encoding = r.apparent_encoding
#         html = etree.HTML(r.text)
#         res = html.xpath('(//table//tbody)[1]//*[@class="al"]//a/text()')
#         reshtml = html.xpath('(//table//tbody)[1]//*[@class="al"]//a/@href')
#         reslen = len(res())
#         # rrhtml = []
#         # for i in range(0,reslen):
#         #     rrhtml += res[i]+'\n'+reshtml[i]
#         # re = '\n- '.join(rrhtml)
#         # ret = '- ' + re
#         return reslen
#     except:
#         return '糟了!访问出错,赶紧找大欣悦来解决问题'


def main():
    print(get_tophub_name(baidu_hotspot))
    
if __name__ == '__main__':
    main()