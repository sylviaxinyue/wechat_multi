# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 13:16:33 2021

@author: Administrator
"""

import requests
import random
from datetime import datetime
import datetime

key = "dbeb0502639ec115136dc52002f8596b"
ttype = 2  

def main():
    print(get_tiangou())

def get_sleep_story():
    url = "http://api.tianapi.com/txapi/story/index"
    params = {
        "key": key,  # 应用APPKEY(应用详细页查询)
        #"type":ttype #故事类型，成语1、睡前2、童话3、寓言4
    }

    try:
        r = requests.get(url = url,params = params)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        res = r.json()
        w = res["newslist"]
        re = w[0]
        title = re['title']
        content = re['content']
        result = '~~睡前故事~~\n《'+title+'》\n'+content
        return result
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题'
    
def get_bulletin():
    url = "http://api.tianapi.com/bulletin/index"
    params = {
        "key": key,  # 应用APPKEY(应用详细页查询)
    }

    try:
        r = requests.get(url = url,params = params)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        res = r.json()
        w = res["newslist"]
        resu_list = []
        for i in range(5):
            re = w[i]
            title = re['title']
            content = re['digest']
            resu = '《'+title+'》\n'+content+'\n'
            resu_list.append(resu)
        resul = '\n'.join(resu_list)
        result = '~~简报五则~~\n'+resul
        return result
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题'
    
def get_tiangou():
    url = "http://api.tianapi.com/txapi/tiangou/index"
    params = {
        "key": key,  # 应用APPKEY(应用详细页查询)
    }

    # try:
    r = requests.get(url = url,params = params)
    r.raise_for_status()
    r.encoding = r.apparent_encoding
    res = r.json()
    w = res["newslist"]
    re = w[0]
    content = re['content']
    weather_list =['晴天','阵雨','小雨','大雨','大雾','阴天','多云']
    weather = ''.join(random.sample(weather_list, 1))
    now_da = datetime.datetime.now()
    now_date = now_da.strftime("%m%d")
    mm = now_date[0:2]+'月'
    dd = now_date[2:4]+'日 '
    result = '舔狗日记 '+mm+dd+weather+'\n'+content
    return result
    # except:
    #     return '糟了!访问出错,赶紧找大欣悦来解决问题' 
        
if __name__ == '__main__':
    main()