# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class TophubspiderItem(scrapy.Item):
    #热点标题
    tophubtitle = scrapy.Field()
    #热点链接
    tophubhref = scrapy.Field()
