import scrapy#导入scrapy


#创建爬虫类 并且继承自scrapy.Spider(爬虫最基础类)
class TophubSpider(scrapy.Spider):
    name = 'tophub'#爬虫名字(必须唯一)
    allowed_domains = ['tophub.today']#允许采集的域名
    baseURL = 'http://tophub.today/n/'
    #热点:[0]百度热点 科技:[1]IT之家 财经:[2]财新网 [3]新浪金融 影视:[4]最新热门电影 [5]影院正在上映 [6]热播电视剧 游戏:[7]3DM资讯 体育:[8饰]新浪体育
    offset = ['Jb0vmloB1G','74Kvx59dkx','3QeLGVPd7k','Ywv4jRxePa','mDOvnyBoEB','m4ejbjyexE','nBe0JLBv37','YqoXQR0vOD','Ywv4jRxePa']
    
    start_urls = [baseURL + str(offset[0])]#开始采集的网站
    
    
    #解析响应数据 提取数据 或者网址
    def parse(self, response):
        #提取数据
        #标题
        tophubtitle = response.xpath('(//table//tbody)[1]//*[@class="al"]//a/text()').
        #网址
        tophubhref = response.xpath('(//table//tbody)[1]//*[@class="al"]//a/@href')