# Wechat_robot微信机器人 

#### 介绍
一个基于PC版本微信软件的机器人程序，可以实现机器人自动对话，彩票查询，新闻阅读，历史上的今天，随机选择，星座黄历查询等功能，目前功能还在逐步增加完善中。


#### 安装及使用教程

1.  安装项目中的PC微信软件，目前只支持windows环境。
2.  安装python37运行环境
3.  运行main.py主程序

#### 参与贡献

1.  感谢WechatPCAPI提供微信接口文件


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
