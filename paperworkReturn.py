# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 13:57:11 2021

@author: Administrator
"""

import requests

def main():
    print(get_paperwork_all())

def get_paperwork_zouxin():
    url = "http://api.tianapi.com/txapi/pyqwenan/index"
    params = {
        "key": 'dbeb0502639ec115136dc52002f8596b',  # 应用APPKEY(应用详细页查询)
    }

    try:
        r = requests.get(url = url,params = params)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        res = r.json()
        w = res["newslist"]
        re = w[0]
        content = re['content']
        source = re['source']
        result = '\n\n简单走心:\n  '+content+' - '+source
        return result
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题'
        
def get_paperwork_wenyi():
    url = "http://api.tianapi.com/txapi/one/index"
    params = {
        "key": 'dbeb0502639ec115136dc52002f8596b',  # 应用APPKEY(应用详细页查询)
    }

    try:
        r = requests.get(url = url,params = params)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        res = r.json()
        w = res["newslist"]
        re = w[0]
        content = re['word']
        source = re['wordfrom']
        result = '\n\n文艺青年:\n  '+content+' - '+source
        return result
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题'  
    
def get_paperwork_film():
    url = "http://api.tianapi.com/txapi/dialogue/index"
    params = {
        "key": 'dbeb0502639ec115136dc52002f8596b',  # 应用APPKEY(应用详细页查询)
    }

    try:
        r = requests.get(url = url,params = params)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        res = r.json()
        w = res["newslist"]
        re = w[0]
        content = re['dialogue']
        english = re['english']
        source = re['source']
        result = '\n\n电影台词:\n  '+content+' '+english+' - '+source
        return result
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题'  
    
def get_paperwork_hard():
    url = "http://api.tianapi.com/txapi/hsjz/index"
    params = {
        "key": 'dbeb0502639ec115136dc52002f8596b',  # 应用APPKEY(应用详细页查询)
    }

    try:
        r = requests.get(url = url,params = params)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        res = r.json()
        w = res["newslist"]
        re = w[0]
        content = re['content']
        result = '\n\n略显沉重:\n  '+content
        return result
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题'  
    
def get_paperwork_wangyi():
    url = "http://api.tianapi.com/txapi/hotreview/index"
    params = {
        "key": 'dbeb0502639ec115136dc52002f8596b',  # 应用APPKEY(应用详细页查询)
    }

    try:
        r = requests.get(url = url,params = params)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        res = r.json()
        w = res["newslist"]
        re = w[0]
        content = re['content']
        source = re['source']
        result = '\n\n网抑热评:\n  '+content+' - 歌名:'+source
        return result
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题'  
    
def get_paperwork_jitang():
    url = "http://api.tianapi.com/txapi/dujitang/index"
    params = {
        "key": 'dbeb0502639ec115136dc52002f8596b',  # 应用APPKEY(应用详细页查询)
    }

    try:
        r = requests.get(url = url,params = params)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        res = r.json()
        w = res["newslist"]
        re = w[0]
        content = re['content']
        result = '\n\n喝毒鸡汤:\n  '+content
        return result
    except:
        return '糟了!访问出错,赶紧找大欣悦来解决问题' 
    
def get_paperwork_all() :
    paperwork_al = '一些文案'+get_paperwork_zouxin()+get_paperwork_wenyi()+get_paperwork_film()+get_paperwork_hard()+get_paperwork_wangyi()+get_paperwork_jitang()+'\n\n发送【10】刷新内容【0】回主菜单'
    # nostr = ['~','','/u3000','/u303F','/uFF00','/uFFEF','/u0000','/u007F','/u201c','/u201d','☉']
    # for i in nostr:
    #     i = ''.join(i)
    #     paperwork_all = paperwork_al.replace(i, " ")
    return paperwork_al
    
    
    
    
    
    
    
    
if __name__ == '__main__':
    main()