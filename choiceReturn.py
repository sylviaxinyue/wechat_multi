# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 21:41:25 2020

@author: Administrator
"""
import random

def main():
    print(style_cooking())#吃什么? 我觉得你可以去吃 共有45种菜品类型,如对结果不满意,可发送【1】重新获取
    print(district_shanghai())#上海上海去哪儿玩? 为你选择了区域与商场,如对结果不满意,可发送【1】重新获取
    print(answer_book())
    print(yn())
    #新环境新路径
    
    #《答案之书》解你心结    天灵灵地灵灵~(施法中)
    # 《答案之书》使用说明
    # 1.把手机放在桌上,闭上眼睛
    # 2.用5到10秒的时间集中思考你的问题
    # 3.在想象或说出你的问题的同时(每次只能有一个问题)在输入框打上【咋办呢】
    # 4.在你感觉时间正确的时候点击发送,你要寻找的答案就被我捯饬来了
    # 5.遇到任何问题你都可以来试试
    
    
    
def style_cooking():
    alwaysHttp = 'http://www.dianping.com/shanghai/ch10/'
    style = (random.choice(['本帮江浙菜!\n%sg101'%alwaysHttp,
                          '日本菜~\n%sg113'%alwaysHttp, 
                          '小吃快餐!\n%sg112'%alwaysHttp,
                          '面包甜点~\n%sg117'%alwaysHttp,
                          '火锅!\n%sg110'%alwaysHttp,
                          '西餐!\n%sg116'%alwaysHttp,
                          '自助餐!\n%sg111'%alwaysHttp,
                          '粤菜!\n%sg103'%alwaysHttp,
                          '韩国料理!\n%sg114'%alwaysHttp,
                          '川菜!\n%sg102'%alwaysHttp,
                          '其他美食..\n%sg118'%alwaysHttp,
                          '咖啡厅~\n%sg132'%alwaysHttp,
                          '小龙虾!\n%sg219'%alwaysHttp,
                          '烧烤烤串!\n%sg508'%alwaysHttp,
                          '东南亚菜!\n%sg115'%alwaysHttp,
                          '饮品店~\n%sg34236'%alwaysHttp,
                          '下午茶~\n%sg34014'%alwaysHttp,
                          '面馆!\n%sg215'%alwaysHttp,
                          '江河湖海鲜!\n%sg251'%alwaysHttp,
                          '东北菜!\n%sg106'%alwaysHttp,
                          '新疆菜!\n%sg3243'%alwaysHttp,
                          '素食!\n%sg109'%alwaysHttp,
                          '湘菜!\n%sg104'%alwaysHttp,
                          '早茶~\n%sg34055'%alwaysHttp,
                          '北京菜!\n%sg311'%alwaysHttp,
                          '农家菜!\n%sg25474'%alwaysHttp,
                          '水果生鲜!\n%sg2714'%alwaysHttp,
                          '家常菜!\n%sg1783'%alwaysHttp,
                          '烤肉!\n%sg34303'%alwaysHttp,
                          '特色菜!\n%sg34284'%alwaysHttp,
                          '私房菜~\n%sg1338'%alwaysHttp,
                          '创意菜!\n%sg250'%alwaysHttp,
                          '中东菜!\n%sg234'%alwaysHttp,
                          '非洲菜!\n%sg2797'%alwaysHttp,
                          '台湾菜!\n%sg107'%alwaysHttp,
                          '福建菜!\n%sg34059'%alwaysHttp,
                          '江西菜!\n%sg247'%alwaysHttp,
                          '徽菜!\n%sg26482'%alwaysHttp,
                          '西北民间菜!\n%sg34235'%alwaysHttp,
                          '鲁菜!\n%sg26483'%alwaysHttp,
                          '贵州菜|黔菜!\n%sg105'%alwaysHttp,
                          '云南菜|滇菜!\n%sg248'%alwaysHttp,
                          '陕菜!\n%sg34234'%alwaysHttp,
                          '湖北菜!\n%sg246'%alwaysHttp,
                          '山西菜!\n%sg26484'%alwaysHttp,
                          '内蒙菜!\n%sg1453'%alwaysHttp,]))
    return(style)

def district_shanghai():
    JAmall = ('\n'.join(random.sample(['静安大悦城',
                                    '兴业太古汇',
                                    '静安嘉里中心',
                                    '静安大融城',
                                    '大宁国际商业广场',
                                    '恒隆广场',
                                    '芮欧百货',
                                    '久光百货',
                                    '晶品CrystalGalleria',
                                    '大宁音乐广场',
                                    '梅龙镇广场',
                                    '上海协信星光广场',
                                    '889广场',
                                    '圣和圣韩国馆',
                                    '凯德星贸 Capital Square'],3)))
    CNmall = ('\n'.join(random.sample(['龙之梦购物公园',
                                    '长宁来福士',
                                    '虹桥南丰城',
                                    '金虹桥国际中心',
                                    '上生新所',
                                    '上海高岛屋百货',
                                    '百盛优客城市广场(天山店)',
                                    '缤谷文化休闲广场',
                                    '尚嘉中心',
                                    '百联西郊购物中心',
                                    '幸福里',
                                    '古北1699商业广场',
                                    '星空广场',
                                    'KING88商业广场(长宁店)',
                                    '巴黎春天(天山店)'],3)))
    XHmall = ('\n'.join(random.sample(['环贸iapm商场',
                                    '港汇恒隆广场',
                                    '美罗城',
                                    '徐汇日月光中心',
                                    '绿地缤纷城',
                                    'One ITC',
                                    '大华光启城时尚购物中心',
                                    '飞洲国际广场',
                                    '正大乐城',
                                    '星游城',
                                    '汇金百货(徐汇店)',
                                    '太平洋百货(徐汇店)',
                                    '保利时光里购物中心',
                                    '汇金奥特莱斯(汇金南站店)',
                                    '南洋1931'],3)))
    YPmall = ('\n'.join(random.sample(['合生汇',
                                    '紫荆广场',
                                    '万达广场(五角场店)',
                                    '百联又一城购物中心',
                                    '上海国际时尚中心',
                                    '百联滨江购物中心',
                                    '悠方',
                                    '宝地广场',
                                    '国华广场',
                                    '旭辉mall',
                                    '东方渔人码头',
                                    '巴黎春天(五角场店)',
                                    '中原城市广场',
                                    '太平洋森活天地',
                                    '君欣·时代广场'],3)))
    HPmall = ('\n'.join(random.sample(['日月光中心广场',
                                    '环贸iapm商场',
                                    'LuOne凯德晶萃广场',
                                    '上海来福士广场',
                                    'BFC外滩金融中心',
                                    '上海K11购物艺术中心',
                                    'TX淮海',
                                    '新世界大丸百货',
                                    '新世界城',
                                    '新天地广场',
                                    '中海环宇荟',
                                    '上海世茂广场',
                                    '第一百货商业中心',
                                    '香港广场',
                                    'SOHO复兴广场'],3)))
    HKmall = ('\n'.join(random.sample(['龙之梦购物中心(虹口店)',
                                    '上海白玉兰广场',
                                    '瑞虹天地',
                                    '百联曲阳购物中心',
                                    '宝地广场',
                                    '圣和圣韩国馆',
                                    '上滨生活广场',
                                    '星荟中心',
                                    '壹丰广场',
                                    '利通广场',
                                    '中信广场',
                                    '瑞虹坊',
                                    '上海ist艾尚天地',
                                    '金融街海伦中心',
                                    '金茂时尚生活中心'],3)))
    PTmall = ('\n'.join(random.sample(['环球港',
                                    '长风大悦城',
                                    '百联中环购物广场',
                                    '近铁城市广场',
                                    '巴黎春天(陕西路店)',
                                    '桃源π商业广场',
                                    'IMAGO我格广场',
                                    '189弄购物中心',
                                    '巴黎春天(宝山店)',
                                    '星光耀广场',
                                    '大华虎城嘉年华广场',
                                    '亚新生活广场',
                                    '金沙和美广场',
                                    '118广场',
                                    '融创精彩天地(香溢店)'],3)))
    MHmall = ('\n'.join(random.sample(['上海万象城',
                                    '七宝万科广场',
                                    '虹桥天地',
                                    '上海爱琴海购物公园',
                                    '中庚·漫游城',
                                    '龙湖上海闵行天街',
                                    '仲盛世界商城',
                                    '龙湖上海虹桥天街',
                                    '莘庄维璟印象城',
                                    '龙之梦购物中心(莘庄店)',
                                    '百联南方购物中心',
                                    '万达广场(闵行颛桥店)',
                                    '万达广场(浦江店)',
                                    '宝龙城(七宝店)',
                                    '浦江城市生活广场'],3)))
    BSmall = ('\n'.join(random.sample(['万达广场(宝山店)',
                                    '宝杨宝龙广场',
                                    '龙湖上海宝山天街',
                                    '经纬汇',
                                    '宝乐汇生活时尚中心',
                                    '巴黎春天(宝山店)',
                                    '宝山花园城',
                                    '正大乐城',
                                    '大华虎城嘉年华广场',
                                    '罗店宝龙广场',
                                    '临港新业坊·源创',
                                    '三邻桥体育文化园',
                                    '诺亚新天地',
                                    '红太阳商业广场',
                                    '宝山U天地',],3)))
    PDmall = ('\n'.join(random.sample(['世纪汇广场',
                                    '国金中心商场',
                                    '上海陆家嘴中心 L+MALL',
                                    '正大广场',
                                    '长泰广场',
                                    'LCM置汇旭辉广场',
                                    '世博源',
                                    '金桥国际商业广场',
                                    '晶耀前滩Crystal Plaza',
                                    '比斯特上海购物村',
                                    '浦东嘉里城',
                                    '复地活力城',
                                    '九六广场',
                                    '华润时代广场',
                                    '新达汇·三林',],3)))
    JSmall = ('\n'.join(random.sample(['万达广场(金山店)',
                                    '百联金山购物中心',
                                    '五福商业广场',
                                    '易家中心(金山店)',
                                    '东方商厦(金山店)',
                                    '枫泾老街商业城',
                                    '光明·koko都乐汇',
                                    '石化百货',
                                    '康隆商业广场',
                                    '港翔休闲广场',
                                    '竟衡88广场',
                                    '金上海生活广场',
                                    '涌金商业广场商铺',
                                    '都乐汇广场',
                                    '蓝色海岸生活广场',],3)))
    SJmall = ('\n'.join(random.sample(['万达广场(松江店)',
                                    '九亭金地广场',
                                    '开元地中海商业广场',
                                    '东鼎购物中心',
                                    '亚繁亚乐城',
                                    '保利悦活荟(松江店)',
                                    '新理想广场',
                                    '五龙商业广场',
                                    '明中广场',
                                    '鹿都国际商业广场',
                                    '旭辉九亭u天地',
                                    '平高广场',
                                    '三湘商业广场',
                                    '塞纳左岸广场',
                                    '马利来广场',],3)))
    JDmall = ('\n'.join(random.sample(['上海南翔印象城MEGA',
                                    '中信泰富万达广场(嘉定新城店)',
                                    '万达广场(江桥店)',
                                    '宝龙广场',
                                    '嘉定大融城',
                                    '嘉亭荟城市生活广场',
                                    '百联嘉定购物中心',
                                    '五彩城',
                                    '中冶祥腾城市广场',
                                    '信业购物中心',
                                    '日月光中心',
                                    '疁城新天地',
                                    '金沙和美广场',
                                    '罗宾森购物中心',
                                    '花都荟',],3)))
    QPmall = ('\n'.join(random.sample(['百联奥特莱斯',
                                    '青浦万达茂广场',
                                    '青浦宝龙广场',
                                    '夏都小镇',
                                    '吾悦广场',
                                    '青浦绿地缤纷城',
                                    '百联悠迈生活广场',
                                    '虹桥·食尚天地',
                                    '合生新天地',
                                    '国家会展中心商业广场',
                                    '东方商厦(青浦店)',
                                    '好特卖HotMaxx(青浦万达茂店)',
                                    '富绅商业中心',
                                    '博隆商业广场',
                                    '东渡蛙城',],3)))
    FXmall = ('\n'.join(random.sample(['百联南桥购物中心',
                                    '奉贤宝龙广场',
                                    '苏宁生活广场',
                                    '连城商业广场',
                                    '金汇商业广场',
                                    '星雨城时代广场',
                                    '宝华帝华商业广场',
                                    '浦商百货(金叶店)',
                                    '恒龙金尊广场',
                                    '金叶商厦业务科',
                                    '南方国际广场',
                                    '南桥国际商业广场北门',
                                    '南桥老街商业广场',
                                    '杭州湾生活广场',
                                    '玫瑰园商贸城',],3)))
    CMmall = ('\n'.join(random.sample(['万达广场(上海崇明店)',
                                    '八一广场',
                                    '名岛生活广场',
                                    '城桥镇新城生活广场',
                                    '百联崇明商业广场',
                                    '辛泓广场',
                                    '欧特福生活购物中心',
                                    '永乐(陈家镇店)',
                                    'ACTREE(崇明万达广场店)',
                                    '长兴时代广场',
                                    '崇明特产',
                                    '长兴百货',
                                    '吉的堡少儿英语八一广场教学点',
                                    '欧特福生活购物中心',
                                    '育麟商务广场'],3)))

    
    district = (random.choice(['静安区\n%s'%JAmall,
                               '长宁区\n%s'%CNmall,
                               '徐汇区\n%s'%XHmall,
                               '杨浦区\n%s'%YPmall,
                               '黄浦区\n%s'%HPmall,
                               '虹口区\n%s'%HKmall,
                               '普陀区\n%s'%PTmall,
                               '闵行区\n%s'%MHmall,
                               '宝山区\n%s'%BSmall,
                               '浦东新区\n%s'%PDmall,
                               '松江区\n%s'%SJmall,
                               '嘉定区\n%s'%JDmall,
                               '青浦区\n%s'%QPmall,
                               '金山区\n%s'%JSmall,
                               '奉贤区\n%s'%FXmall,
                               '崇明区\n%s'%CMmall,
                               ]))
    return(district) 

def answer_book():
    number=str((random.randint(7, 324)))
    photo_number=number.zfill(3)
    photo_location=('C:\\python\\answer\\answer-%s.jpg'%photo_number)
    return(photo_location)                          
                                 
                                 
                                 
    #wx_inst.send_img(to_user='filehelper', img_abspath=r'C:\Users\Leon\Pictures\1.jpg')
    
def yn():
    YN = (random.choice(['Yes!','No~']))
    return(YN)
    
    
    
    
    
    
    
    
    
    
    
if __name__ == '__main__':
    main()