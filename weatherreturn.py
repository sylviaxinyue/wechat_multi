# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 14:08:18 2020

@author: Administrator
"""
'''url
实况
        https://api.seniverse.com/v3/weather/now.json
        key=SWcCOJEl1fQMYGeEu&location=beijing&language=zh-Hans&unit=c
        
今天和未来 4 天的预报 
        https://api.seniverse.com/v3/weather/daily.json
        key=SWcCOJEl1fQMYGeEu&location=beijing&language=zh-Hans&unit=c&start=0&days=5
        
生活指数      
        https://api.seniverse.com/v3/life/suggestion.json
        key=SWcCOJEl1fQMYGeEu&location=shanghai&language=zh-Hans

北京现在....
生活指数...

未来三天天气预报...
'''

import requests
import sys

#接口网址
now_weather_url = 'https://api.seniverse.com/v3/weather/now.json'
daily_weather_url = 'https://api.seniverse.com/v3/weather/daily.json'
life_url = 'https://api.seniverse.com/v3/life/suggestion.json'
key = 'SWcCOJEl1fQMYGeEu'

#中文变为拼音进行查询
def getLocation():
    LOCATION = '上海'
    """get location from user input"""
    argvs = sys.argv
    location = argvs[1] if len(argvs) >= 2 else LOCATION
    return location

#分析接口
def fetchWeather(city):
    URL = now_weather_url
    res = requests.get(URL, params={
        'key': key,
        'location': city,
    }, timeout=1)
    w = res.json()
    for one in w:
        if 'status' in w:
            return('0')
            pass
        else:
            # 成功请求
            now_weather_or = w["results"][0]
            daytime = ''.join(now_weather_or.get('last_update'))
            date = daytime[5:10]
            time = daytime[11:16]
            now_weater_tuple = ('%s * 日期:%s * 时间:%s' % (now_weather_or.get('location').get('name'),date,time),'\n',
                                '天气状况:%s · %s度' % (now_weather_or.get('now').get('text'),now_weather_or.get('now').get('temperature')),'\n'
                                )
            now_weater_str = ''.join(now_weater_tuple)
          
            URL = life_url
            res = requests.get(URL, params={
                'key': key,
                'location': city,
            }, timeout=1)
    
            w = res.json()
            life_or = w["results"][0]
            life_tuple = ('↓↓↓生活指数↓↓↓','\n',
                          '今天体感%s,' % life_or.get('suggestion').get('dressing').get('brief'),
                          '%s生感冒.' % life_or.get('suggestion').get('flu').get('brief'),'\n',
                          '紫外线指数%s,' % life_or.get('suggestion').get('uv').get('brief'),
                          '%s出门走动,' % life_or.get('suggestion').get('sport').get('brief'),
                          '%s洗车.' % life_or.get('suggestion').get('car_washing').get('brief'),)
            life_str = ''.join(life_tuple)  
                        
            return  now_weater_str + life_str
        
def futureWeather(city):
    URL = daily_weather_url
    res = requests.get(URL, params={
        'key': key,
        'location': city,
    }, timeout=1)
    w = res.json()
    for one in w:
        if 'status' in w:
            return('0')
            pass
        else:
            daily_or1 = w["results"][0].get('daily')[0]
            daytime1 = ''.join(daily_or1.get('date'))
            date1 = daytime1[5:10]
            daily_or2 = w["results"][0].get('daily')[1]
            daytime2 = ''.join(daily_or2.get('date'))
            date2 = daytime2[5:10]
            daily_or3 = w["results"][0].get('daily')[2]
            daytime3 = ''.join(daily_or3.get('date'))
            date3 = daytime3[5:10]
            daily_tuple =('近三日天气概况:\n',
                          '日期:%s * 今 \n白天%s,夜晚%s,气温%s/%s度,降雨概率%s,%s风%s级,湿度%s\n' % (date1,daily_or1.get('text_day'),daily_or1.get('text_night'),daily_or1.get('high'),daily_or1.get('low'),daily_or1.get('rainfall'),daily_or1.get('wind_direction'),daily_or1.get('wind_scale'),daily_or1.get('humidity')),
                          '日期:%s * 明 \n白天%s,夜晚%s,气温%s/%s度,降雨概率%s,%s风%s级,湿度%s\n' % (date2,daily_or2.get('text_day'),daily_or2.get('text_night'),daily_or2.get('high'),daily_or2.get('low'),daily_or2.get('rainfall'),daily_or2.get('wind_direction'),daily_or2.get('wind_scale'),daily_or2.get('humidity')),
                          '日期:%s * 后 \n白天%s,夜晚%s,气温%s/%s度,降雨概率%s,%s风%s级,湿度%s\n' % (date3,daily_or3.get('text_day'),daily_or3.get('text_night'),daily_or3.get('high'),daily_or3.get('low'),daily_or3.get('rainfall'),daily_or3.get('wind_direction'),daily_or3.get('wind_scale'),daily_or3.get('humidity')),
                          )
            daily_str = ''.join(daily_tuple)
            return daily_str
    
        
if __name__ == '__main__':
    location = getLocation()
    result = diaryWeather()
    print(result)