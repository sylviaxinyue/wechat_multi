# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 21:50:42 2020

@author: Administrator
"""

from selenium import webdriver
import time

def main():
    print(topsearch())

def topsearch():
    url = 'https://s.weibo.com/top/summary?cate=realtimehot'
    browser = webdriver.Chrome()
    browser.get(url)
    
    namelist = []
    
    time.sleep(3)
    
    element = browser.find_element_by_class_name('data')
    elements = element.find_elements_by_tag_name('a')
    
    for element in elements:
        namelist.append(element.text)
        pass
    
    browser.close()
    
    namestr = '\n- '.join(namelist)
    
    return namestr
if __name__ == '__main__':
    main()