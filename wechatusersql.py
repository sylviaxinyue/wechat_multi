# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 14:52:45 2020

@author: Administrator

"""

'''
使用逻辑
用户名键=from_wxid

收到消息
获取用户名
查询数据库用户名 
返回数据库用户名
比对用户名和数据库用户名
if 用户名存在
    pass
if用户名不存在
    添加用户
查询该用户指针信息
返回指针信息
根据指针信息运行菜单

菜单发生变更
获取用户名
变更该用户指针信息
'''


'''开始创建对象'''
from pymysql import connect

def main():
    wechat_user = ('wxid_hvkd62gbmjx112')
    gstate = ('state_list.main')
    #创建对象
    mcsql = MCsql()
    #调用方法
    
    
    a = mcsql.show_subs_users('subs_lottery_switch')
    print(mcsql.show_subs_user_switch('subs_lottery_switch',wechat_user))
    
class MCsql(object):
#---------------------------------------------------------------------
#分别 是进入数据库,退出且保存数据库,语言转换 方法
    def __init__(self):
        #建立连接
        self.conn = connect(host='localhost',port=3306,user='root',password='710412',database='multi_customer',charset='utf8')
        #获得Cursor对象
        self.curs = self.conn.cursor()
    
    def __del__(self):
        #关闭Cursor对象
        self.curs.close()
        self.conn.close()
        
    def execute_sql(self,sql):
        #使用sql语句
        self.curs.execute(sql)
        listsql = []
        for temp in self.curs.fetchall():
            strtemp = ''.join(temp)
            listsql.append(strtemp)
        return listsql
    
#----------------------------------------------------------------------
#微信用户菜单位置方法合集
    
    def show_wechat_name(self):
        #获取所有微信ID（list)  ok
        sql = "select `wechat_name` from wechatuser"
        names = self.execute_sql(sql)
        return names
    
    def add_wechat_name(self,user):
        #写入数据库微信ID  ok
        sql = """insert into wechatuser (wechat_name) values("%s")""" % user
        self.curs.execute(sql)
        self.conn.commit()
        pass
    
    def show_location(self,user):
        #获取指定id的菜单指针(str)  ok
        sql = """select `locations` from wechatuser where `wechat_name`= ("%s")""" % user
        location = self.execute_sql(sql)
        location = ''.join(location)
        return location

    def add_location(self,location,user):
        #修改指定id的菜单指针  ok
        sql = """update wechatuser set `locations` = ("%s") where `wechat_name` = ("%s")""" % (location,user)
        self.curs.execute(sql)
        self.conn.commit()
        pass
    
#----------------------------------------------------------------------
#微信用户个人信息方法合集
    def show_userdetail(self,detail,user):
        #获取指定id的个人信息(str)  ok
        sql = """select `%s` from wechatuser where `wechat_name`= ("%s")""" % (detail,user)
        location = self.execute_sql(sql)
        location = ''.join(location)
        return location
    
    def add_userdetail(self,detail,location,user):
        #修改指定id的个人信息  ok
        sql = """update wechatuser set `%s` = ("%s") where `wechat_name` = ("%s")""" % (detail,location,user)
        self.curs.execute(sql)
        self.conn.commit()
        pass
#----------------------------------------------------------------------------
#房源网站方法合集
    def show_house_name(self):
        #获取所有小区名称（list)  ok
        sql = "select `house_name` from fangyuan"
        names = self.execute_sql(sql)
        return names
    
    def add_house_tag(self,number,user):
        #修改指定小区tag  ok
        sql = """update fangyuan set `house_tag` = ("%s") where `house_name` = ("%s")""" % (number,user)
        self.curs.execute(sql)
        self.conn.commit()
        pass
    
    def add_house_name(self,user):
        #写入数据库小区名称  ok
        sql = """insert into fangyuan (house_name) values("%s")""" % user
        self.curs.execute(sql)
        self.conn.commit()
        pass
    
    def show_house_new_namelist(self):
        #获取新增小区名称（list)  ok
        sql = "select `house_name` from fangyuan where `house_tag` = 1"
        names = self.execute_sql(sql)
        return names
    
    def show_house_focus_namelist(self):
        #获取新增小区名称（list)  ok
        sql = "select `house_name` from fangyuan where `house_tag` != 3 and `house_focus` = 1"
        names = self.execute_sql(sql)
        return names
    
    def end_house_tag(self):
        #修改所有小区tag  ok
        sql = """update fangyuan set `house_tag` = 3 """ 
        self.curs.execute(sql)
        self.conn.commit()
        pass
    
    def show_compar_house_name(self):
        #获取上次的房源列表  ok
        sql = "select `house_name` from fangyuan where `house_tag` != 3"
        names = self.execute_sql(sql)
        return names
    
    def show_compar_dis_house_name(self):
        #获取上次的房源列表  ok
        sql = "select `house_name` from fangyuan where `house_tag` != 3 and `house_tag` != 4"
        names = self.execute_sql(sql)
        return names
    
#----------------------------------------------------------------------------
#天气定位方法合集
    def show_weather_city(self,user):
        #获取指定id的天气定位(str)  ok
        sql = """select `weather_city` from wechatuser where `wechat_name`= ("%s")""" % user
        city = self.execute_sql(sql)
        city = ''.join(city)
        return city
    
    def add_weather_city(self,location,user):
        #修改指定id的天气定位  ok
        sql = """update wechatuser set `weather_city` = ("%s") where `wechat_name` = ("%s")""" % (location,user)
        self.curs.execute(sql)
        self.conn.commit()
        pass
    
#---------------------------------------------------------------------------------------------------------
#自动发送信息方法合集
    def show_subs_switch(self,detail):
        #获取该locationg功能的开关状态
        sql = """select `%s` from wechatuser """ % (detail)
        location = self.execute_sql(sql)
        location = ''.join(location)
        return location
    
    def show_subs_users(self,switch):
        #获取订阅者的id
        sql = """select `wechat_name` from wechatuser where `%s` = ('1')""" % switch
        location = self.execute_sql(sql)
        return location
    
    def show_subs_usetime(self,detail,user):
        #获取订阅者的订阅时间
        sql = """select `%s` from wechatuser where `wechat_name` = ('%s') """ % (detail,user)
        location = self.execute_sql(sql)
        location = ''.join(location)
        return location
    
    def show_subs_user_switch(self,detail,user):
        #获取某用户某功能的开关状态
        sql = """select `%s` from wechatuser where `wechat_name` = ('%s')""" % (detail,user)
        location = self.execute_sql(sql)
        location = ''.join(location)
        return location

#用户订阅方法合集
    def add_subs_switch(self,detail,location,user):
        #修改某用户某功能的开关
        sql = """update wechatuser set `%s` = ("%s") where `wechat_name` = ("%s")""" % (detail,location,user)
        self.curs.execute(sql)
        self.conn.commit()
        pass







    
if __name__ =="__main__":
    main()
    
    
    
    
    
    
    
    

    