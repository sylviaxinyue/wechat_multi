# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:50:06 2020

@author: Administrator
"""
'''
数据库标记:
    house_tag=1:新增房源
    house_tag=2:原有房源
    house_tag=3:消失房源
    house_tag=4:较上次重复房源
    house_focus=1:关注房源(存在)
    
逻辑:
    查询房源到list
    比较上次的房源(house_tag != 3)
        分别查看是否存在于上一次的查询
            存在就把house_tag改为4
            不存在列入较上次新增:compar_new_namelist        
    compar_new_namelist = ...
    compar_dis_namelist = house_tag 不为3,不为4
    house_tag全部变为3
    分别查看每一个在数据库是否存在
        不存在就加入,默认house_tag为1
        存在把house_tag改为2
    new_namelist = 查询house_tag为1的房源
    return_namelist = 在compar_new_namelist但是不在new_namelist的回归房源
    focus_namelist = 查询house_focus为1,house_tag不为3的房源
    

'''
from selenium import webdriver
import time
from wechatusersql import MCsql

def main():
    print(fangyuan())

def fangyuan():
#----------------------------------------------------------------------
#获取网页信息
    url = 'https://select.pdgzf.com/villageLists'
    browser = webdriver.Chrome()
    browser.get(url)
    
    namelist = []
    
    time.sleep(3)
    
    element = browser.find_element_by_class_name('village-house-wrap')
    elements = element.find_elements_by_tag_name('h4')
    
    for element in elements:
        namelist.append(element.text)
        pass
    
    bottom = browser.find_element_by_class_name('btn-next')
    bottom.click()
    
    time.sleep(3)
    
    element = browser.find_element_by_class_name('village-house-wrap')
    elements = element.find_elements_by_tag_name('h4')
    
    for element in elements:
        namelist.append(element.text)
        pass
    
    browser.close()
#------------------------------------------------------------------------  
#信息分析 
    namelist = list(set(namelist))
    countall = len(namelist)
    mcsql = MCsql()
    sql_namelist = mcsql.show_house_name()
    sql_compar_namelist = mcsql.show_compar_house_name()
#比较上次的房源
    compar_new_namelist = []
    for i in namelist:
        if i in sql_compar_namelist:#如果存在则把house_tag改为4
            mcsql.add_house_tag('4',i)
            pass
        if i not in sql_compar_namelist:#如果不存在则加入新增列表
            compar_new_namelist.append(i)
            pass
    compar_dis_namelist = mcsql.show_compar_dis_house_name()

#重置房源状态
    mcsql.end_house_tag()
  
#录入查询的房源
    for ii in namelist:
        if ii in sql_namelist:#如果存在则把house_tag改为2
            mcsql.add_house_tag('2',ii)  
        pass
        if ii not in sql_namelist:#如果不存在则加入
            mcsql.add_house_name(ii)
        pass
    new_namelist = mcsql.show_house_new_namelist()
    countnew = len(new_namelist)
    return_namelist =  [ iii for iii in compar_new_namelist if iii not in new_namelist ]
    focus_namelist = mcsql.show_house_focus_namelist()
    countfocus = len(focus_namelist)

    
#输出内容整理
    namestr = '\n'.join(namelist)
    new_namestr = '\n'.join(new_namelist)
    return_namestr = '\n'.join(return_namelist)
    compar_dis_namestr = '\n'.join(compar_dis_namelist)
    focus_namestr = '\n'.join(focus_namelist)
    nameprint = '共%s个小区信息:\n'% countall + namestr + '\n\n回归小区:\n' + return_namestr + '\n\n较上次消失小区:\n' + compar_dis_namestr + '\n\n新增小区%s个:\n'% countnew + new_namestr + '\n\n尚存关注小区%s个:\n'% countfocus + focus_namestr +'\nhttps://select.pdgzf.com/villageLists'
    
    return nameprint
if __name__ == '__main__':
    main()